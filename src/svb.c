#include "svb.h"
#include <sfile/sfile.h>
#include <sfile/ssf.h>
#include <string.h>


static uint16_t SVExec_SVB_loadBE16(uint8_t *data)
{
    return data[0] << 8 | data[1];
}


SVExec_MemoryObject* SVExec_SVB_load(uint8_t const *data, size_t size)
{
    if(!data || !size)
        return NULL;
    SFile_SSFTree *tree = SFile_ssfTree_decode(data, size);
    if(!tree)
        goto error;
    if(strncmp(tree->name, "SVB ", 4) != 0)
        goto error_tree;
    SFile_SSFChunk **chunks = NULL;
    size_t chunkCount = 0;
    SFile_ssfTree_getChunks(tree, "HEAD", &chunks, &chunkCount);
    if(!chunks || !chunkCount)
        goto error_tree;
    if(chunks[0]->length != 10)
        goto error_chunks;
    uint16_t constDataSize = SVExec_SVB_loadBE16(chunks[0]->data);
    uint16_t varDataSize = SVExec_SVB_loadBE16(chunks[0]->data + 2);
    uint16_t varDataStart = SVExec_SVB_loadBE16(chunks[0]->data + 4);
    uint16_t stackStart = SVExec_SVB_loadBE16(chunks[0]->data + 6);
    uint16_t stackSize = SVExec_SVB_loadBE16(chunks[0]->data + 8);
    if(constDataSize + varDataSize > 0x10000 ||
            varDataStart < constDataSize ||
            varDataSize + varDataStart > 0x10000 ||
            stackStart < varDataStart ||
            stackStart + stackSize > varDataStart + varDataSize)
        goto error_chunks;

    FFREE(chunks);
    chunks = NULL;
    chunkCount = 0;
    SFile_ssfTree_getChunks(tree, "DATA", &chunks, &chunkCount);
    if(!chunks || !chunkCount)
        goto error_tree;
    if(chunks[0]->length != constDataSize)
        goto error_chunks;
    SVExec_MemoryObject *memory = SVExec_memoryObject_create(chunks[0]->data, constDataSize, varDataSize);
    if(!memory)
        goto error_chunks;
    memory->varDataStart = varDataStart;
    memory->stackStart = stackStart;
    memory->stackSize = stackSize;

    FFREE(chunks);
    chunks = NULL;
    chunkCount = 0;
    SFile_ssfTree_getChunks(tree, "EXPT", &chunks, &chunkCount);
    if(!chunks || !chunkCount)
        goto error_tree;
    size_t index = 0;
    while(1)
    {
        if(index + 4 > chunks[0]->length)
            break;
        uint16_t address = SVExec_SVB_loadBE16(chunks[0]->data + index);
        size_t nameLen = strnlen((char*)(chunks[0]->data + index + 2), chunks[0]->length - index - 2);
        char *name = NULL;
        MMALLOC(name, char, nameLen + 1);
        strncpy(name, (char*)(chunks[0]->data + index + 2), nameLen);
        name[nameLen] = '\0';
        SVExec_Export *export = SVExec_export_create(name, address);
        FFREE(name);
        SVExec_exportList_add(memory->exports, export);
        index += nameLen + 3;
    }

    FFREE(chunks);
    SFile_ssfTree_destroy(tree);
    return memory;

    error_chunks:
    FFREE(chunks);
    error_tree:
    SFile_ssfTree_destroy(tree);
    error:
    return NULL;
}
