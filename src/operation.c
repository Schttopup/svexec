/**
 * \file operation.c
 * \brief Defines data structures and functions related to bytecode operations.
 * \author Schttopup
 */

#include "operation.h"
#include "processor.h"

#define SVEXEC_OP_MEMCHK(ptr) if(!ptr){processor->error = SVEXEC_ERROR_MEMORY; return;}


// Function is duplicated from processor.c for inlining
static inline __attribute__((always_inline)) uint8_t* SVExec_processor_getMemoryReadInl(SVExec_Processor *processor, uint16_t address, uint16_t size)
{
    SVExec_MemoryObject *memory = processor->context->memory;
    uint16_t varStart = memory->varDataStart;
    if(address >= varStart) // Variable block
    {
        if(address - varStart <= memory->varDataSize - size)
            return processor->context->variables + address - varStart;
    }
    else // Constant block
    {
        if(address <= memory->constDataSize - size)
            return memory->constData + address;
    }
    return NULL;
}

// Function is duplicated from processor.c for inlining
static inline __attribute__((always_inline)) uint8_t* SVExec_processor_getMemoryWriteInl(SVExec_Processor *processor, uint16_t address, uint16_t size)
{
    SVExec_MemoryObject *memory = processor->context->memory;
    uint16_t varStart = memory->varDataStart;
    if(address >= varStart) // Variable block
    {
        if(address - varStart <= memory->varDataSize - size)
            return processor->context->variables + address - varStart;
    }
    else // Constant block
        return NULL;
    return NULL;
}


void SVExec_operation_nop(SVExec_Processor *processor)
{
    processor->programCounter++;
}

void SVExec_operation_ldimm8(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 2);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t reg = (uint8_t) (*(mem + 1) & 0b00001111);
    *(processor->registers + reg) = *mem;
    processor->programCounter += 3;
}

void SVExec_operation_ldimm16(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 3);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t reg = (uint8_t) (*(mem + 2) & 0b00001110);
    *(processor->registers + reg) = *mem;
    *(processor->registers + reg + 1) = *(mem + 1);
    processor->programCounter += 4;
}

void SVExec_operation_ldmem8(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 3);
    SVEXEC_OP_MEMCHK(mem);
    uint16_t memOffset = (*mem << 8) + *(mem + 1);
    uint8_t *memPtr = SVExec_processor_getMemoryReadInl(processor, memOffset, 1);
    uint8_t reg = (uint8_t) (*(mem + 2) & 0b00001111);
    SVEXEC_OP_MEMCHK(memPtr);
    *(processor->registers + reg) = *memPtr;
    processor->programCounter += 4;
}

void SVExec_operation_ldmem16(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 3);
    SVEXEC_OP_MEMCHK(mem);
    uint16_t memOffset = (*mem << 8) + *(mem + 1);
    uint8_t *memPtr = SVExec_processor_getMemoryReadInl(processor, memOffset, 1);
    uint8_t reg = (uint8_t) (*(mem + 2) & 0b00001110);
    SVEXEC_OP_MEMCHK(memPtr);
    *(processor->registers + reg) = *memPtr;
    *(processor->registers + reg + 1) = *(memPtr + 1);
    processor->programCounter += 4;
}

void SVExec_operation_stmem8(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 3);
    SVEXEC_OP_MEMCHK(mem);
    uint16_t memOffset = (*mem << 8) + *(mem + 1);
    if(memOffset >= 0x8000)
    {
        uint8_t *memPtr = SVExec_processor_getMemoryWriteInl(processor, memOffset, 1);
        uint8_t reg = (uint8_t) (*(mem + 2) & 0b00001111);
        SVEXEC_OP_MEMCHK(memPtr);
        *memPtr = *(processor->registers + reg);
        processor->programCounter += 4;
    }
    else
    {
        processor->error = SVEXEC_ERROR_MEMORY;
        return;
    }
}

void SVExec_operation_stmem16(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 3);
    SVEXEC_OP_MEMCHK(mem);
    uint16_t memOffset = (*mem << 8) + *(mem + 1);
    if(memOffset >= 0x8000)
    {
        uint8_t *memPtr = SVExec_processor_getMemoryWriteInl(processor, memOffset, 2);
        uint8_t reg = (uint8_t) (*(mem + 2) & 0b00001110);
        SVEXEC_OP_MEMCHK(memPtr);
        *memPtr = *(processor->registers + reg);
        *(memPtr + 1) = *(processor->registers + reg + 1);
        processor->programCounter += 4;
    }
    else
    {
        processor->error = SVEXEC_ERROR_MEMORY;
        return;
    }
}

void SVExec_operation_ldmem8r(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint16_t memOffset = *(processor->registers + (uint8_t) (*mem & 0b00001110));
    uint8_t *memPtr = SVExec_processor_getMemoryReadInl(processor, memOffset, 1);
    uint8_t reg = (uint8_t) (*mem & 0b11110000);
    SVEXEC_OP_MEMCHK(memPtr);
    *(processor->registers + reg) = *memPtr;
    processor->programCounter += 2;
}

void SVExec_operation_ldmem16r(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint16_t memOffset = *(processor->registers + (uint8_t) (*mem & 0b00001110));
    uint8_t *memPtr = SVExec_processor_getMemoryReadInl(processor, memOffset, 2);
    uint8_t reg = (uint8_t) (*mem & 0b11110000);
    SVEXEC_OP_MEMCHK(memPtr);
    *(processor->registers + reg) = *memPtr;
    *(processor->registers + reg + 1) = *(memPtr + 1);
    processor->programCounter += 2;
}

void SVExec_operation_stmem8r(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t memReg = (uint8_t) (*mem & 0b00001110);
    uint16_t offset = (*(processor->registers + memReg) << 8) + *(processor->registers + memReg + 1);
    if(offset >= 0x8000)
    {
        uint8_t *memPtr = SVExec_processor_getMemoryWriteInl(processor, offset, 1);
        uint8_t reg = (uint8_t) (*mem & 0b11110000);
        SVEXEC_OP_MEMCHK(memPtr);
        *memPtr = *(processor->registers + reg);
    }
    else
    {
        processor->error = SVEXEC_ERROR_MEMORY;
        return;
    }
    processor->programCounter += 2;
}

void SVExec_operation_stmem16r(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t memReg = (uint8_t) (*mem & 0b00001110);
    uint16_t offset = (*(processor->registers + memReg) << 8) + *(processor->registers + memReg + 1);
    if(offset >= 0x8000)
    {
        uint8_t *memPtr = SVExec_processor_getMemoryWriteInl(processor, offset, 2);
        uint8_t reg = (uint8_t) (*mem & 0b11110000);
        SVEXEC_OP_MEMCHK(memPtr);
        *memPtr = *(processor->registers + reg);
        *(memPtr + 1) = *(processor->registers + reg + 1);
    }
    else
    {
        processor->error = SVEXEC_ERROR_MEMORY;
        return;
    }
    processor->programCounter += 2;
}

void SVExec_operation_ldmem8a(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 3);
    SVEXEC_OP_MEMCHK(mem);
    uint16_t memOffset = (*mem << 8) + *(mem + 1);
    uint8_t *memPtr = SVExec_processor_getMemoryReadInl(processor, memOffset, 2);
    SVEXEC_OP_MEMCHK(memPtr);
    uint16_t memOffset2 = (*memPtr << 8) + *(memPtr + 1);
    uint8_t *memPtr2 = SVExec_processor_getMemoryReadInl(processor, memOffset2, 1);
    SVEXEC_OP_MEMCHK(memPtr2);
    uint8_t reg = (uint8_t) (*(mem + 2) & 0b00001111);
    *(processor->registers + reg) = *memPtr2;
    processor->programCounter += 4;
}

void SVExec_operation_ldmem16a(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 3);
    SVEXEC_OP_MEMCHK(mem);
    uint16_t memOffset = (*mem << 8) + *(mem + 1);
    uint8_t *memPtr = SVExec_processor_getMemoryReadInl(processor, memOffset, 2);
    SVEXEC_OP_MEMCHK(memPtr);
    uint16_t memOffset2 = (*memPtr << 8) + *(memPtr + 1);
    uint8_t *memPtr2 = SVExec_processor_getMemoryReadInl(processor, memOffset2, 1);
    SVEXEC_OP_MEMCHK(memPtr2);
    uint8_t reg = (uint8_t) (*(mem + 2) & 0b00001110);
    *(processor->registers + reg) = *memPtr2;
    *(processor->registers + reg + 1) = *(memPtr2 + 1);
    processor->programCounter += 4;
}

void SVExec_operation_stmem8a(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 3);
    SVEXEC_OP_MEMCHK(mem);
    uint16_t memOffset = (*mem << 8) + *(mem + 1);
    uint8_t *memPtr = SVExec_processor_getMemoryReadInl(processor, memOffset, 2);
    SVEXEC_OP_MEMCHK(memPtr);
    uint16_t memOffset2 = (*mem << 8) + *(mem + 1);
    if(memOffset2 >= 0x8000)
    {
        uint8_t *memPtr2 = SVExec_processor_getMemoryWriteInl(processor, memOffset2, 1);
        SVEXEC_OP_MEMCHK(memPtr2);
        uint8_t reg = (uint8_t) (*(mem + 2) & 0b00001111);
        *memPtr2 = *(processor->registers + reg);
        processor->programCounter += 4;
    }
    else
    {
        processor->error = SVEXEC_ERROR_MEMORY;
        return;
    }
}

void SVExec_operation_stmem16a(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 3);
    SVEXEC_OP_MEMCHK(mem);
    uint16_t memOffset = (*mem << 8) + *(mem + 1);
    uint8_t *memPtr = SVExec_processor_getMemoryReadInl(processor, memOffset, 2);
    SVEXEC_OP_MEMCHK(memPtr);
    uint16_t memOffset2 = (*mem << 8) + *(mem + 1);
    if(memOffset2 >= 0x8000)
    {
        uint8_t *memPtr2 = SVExec_processor_getMemoryWriteInl(processor, memOffset2, 1);
        SVEXEC_OP_MEMCHK(memPtr2);
        uint8_t reg = (uint8_t) (*(mem + 2) & 0b00001111);
        *memPtr2 = *(processor->registers + reg);
        *(memPtr2 + 1) = *(processor->registers + reg + 1);
        processor->programCounter += 4;
    }
    else
    {
        processor->error = SVEXEC_ERROR_MEMORY;
        return;
    }
}

void SVExec_operation_mvreg8(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t regA = (uint8_t) (*mem & 0b00001111);
    uint8_t regB = (uint8_t) ((*mem & 0b11110000) >> 4);
    *(processor->registers + regA) = *(processor->registers + regB);
    processor->programCounter += 2;
}

void SVExec_operation_mvreg16(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t regA = (uint8_t) (*mem & 0b00001110);
    uint8_t regB = (uint8_t) ((*mem & 0b11100000) >> 4);
    *(uint16_t *) (processor->registers + regA) = *(uint16_t *) (processor->registers + regB);
    processor->programCounter += 2;
}

void SVExec_operation_mvpc(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t reg = (uint8_t) (*mem & 0b00001110);
    uint8_t val1 = (uint8_t) (processor->programCounter & 0xFF);
    uint8_t val2 = (uint8_t) ((processor->programCounter >> 8) & 0xFF);
    *(processor->registers + reg) = val1;
    *(processor->registers + reg + 1) = val2;
    processor->programCounter += 2;
}

void SVExec_operation_mvsp(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t reg = (uint8_t) (*mem & 0b00001110);
    uint8_t val1 = (uint8_t) (processor->stackPointer & 0xFF);
    uint8_t val2 = (uint8_t) ((processor->stackPointer >> 8) & 0xFF);
    *(processor->registers + reg) = val1;
    *(processor->registers + reg + 1) = val2;
    processor->programCounter += 2;
}

void SVExec_operation_not8(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t reg = (uint8_t) (*mem & 0b00001111);
    *(processor->registers + 1) = ~*(processor->registers + reg);
    processor->programCounter += 2;
}

void SVExec_operation_not16(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t reg = (uint8_t) (*mem & 0b00001110);
    *(uint16_t *) (processor->registers) = ~*(uint16_t *) (processor->registers + reg);
    processor->programCounter += 2;
}

void SVExec_operation_or8(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t regA = (uint8_t) (*mem & 0b00001111);
    uint8_t regB = (uint8_t) ((*mem & 0b11110000) >> 4);
    *(processor->registers + 1) = *(processor->registers + regA) | *(processor->registers + regB);
    processor->programCounter += 2;
}

void SVExec_operation_or16(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t regA = (uint8_t) (*mem & 0b00001110);
    uint8_t regB = (uint8_t) ((*mem & 0b11100000) >> 4);
    *(uint16_t *) (processor->registers) =
            *(uint16_t *) (processor->registers + regA) | *(uint16_t *) (processor->registers + regB);
    processor->programCounter += 2;
}

void SVExec_operation_and8(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t regA = (uint8_t) (*mem & 0b00001111);
    uint8_t regB = (uint8_t) ((*mem & 0b11110000) >> 4);
    *(processor->registers + 1) = *(processor->registers + regA) & *(processor->registers + regB);
    processor->programCounter += 2;
}

void SVExec_operation_and16(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t regA = (uint8_t) (*mem & 0b00001110);
    uint8_t regB = (uint8_t) ((*mem & 0b11100000) >> 4);
    *(uint16_t *) (processor->registers) =
            *(uint16_t *) (processor->registers + regA) & *(uint16_t *) (processor->registers + regB);
    processor->programCounter += 2;
}

void SVExec_operation_xor8(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t regA = (uint8_t) (*mem & 0b00001111);
    uint8_t regB = (uint8_t) ((*mem & 0b11110000) >> 4);
    *(processor->registers + 1) = *(processor->registers + regA) ^ *(processor->registers + regB);
    processor->programCounter += 2;
}

void SVExec_operation_xor16(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t regA = (uint8_t) (*mem & 0b00001110);
    uint8_t regB = (uint8_t) ((*mem & 0b11100000) >> 4);
    *(uint16_t *) (processor->registers) =
            *(uint16_t *) (processor->registers + regA) & *(uint16_t *) (processor->registers + regB);
    processor->programCounter += 2;
}

void SVExec_operation_shl8(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t reg = (uint8_t) (*mem & 0b00001111);
    processor->carryFlag = *(processor->registers + reg) & 0x80 >> 7;
    *(processor->registers + 1) = *(processor->registers + reg) << 1;
    processor->programCounter += 2;
}

void SVExec_operation_shl16(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t reg = (uint8_t) (*mem & 0b00001111);
    processor->carryFlag = (*(processor->registers + reg) & 0x80) >> 7;
    bool hc = (*(processor->registers + reg + 1) & 0x80) >> 7;
    *(processor->registers) = *(processor->registers + reg) << 1 | hc;
    *(processor->registers + 1) = *(processor->registers + reg + 1) << 1;
    processor->programCounter += 2;
}

void SVExec_operation_shr8(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t reg = (uint8_t) (*mem & 0b00001111);
    processor->carryFlag = *(processor->registers + reg) & 1;
    *(processor->registers + 1) = *(processor->registers + reg) >> 1;
    processor->programCounter += 2;
}

void SVExec_operation_shr16(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t reg = (uint8_t) (*mem & 0b00001111);
    processor->carryFlag = *(processor->registers + reg + 1) & 1;
    bool hc = *(processor->registers + reg) & 1;
    *(processor->registers + 1) = *(processor->registers + reg + 1) >> 1 | (hc << 7);
    *(processor->registers) = *(processor->registers + reg) >> 1;
    processor->programCounter += 2;
}

void SVExec_operation_rotl8(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t reg = (uint8_t) (*mem & 0b00001111);
    bool c = processor->carryFlag = (*(processor->registers + reg) & 0x80) >> 7;
    *(processor->registers + 1) = *(processor->registers + reg) << 1 | c;
    processor->programCounter += 2;
}

void SVExec_operation_rotl16(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t reg = (uint8_t) (*mem & 0b00001111);
    bool c = processor->carryFlag = (*(processor->registers + reg) & 0x80) >> 7;
    bool hc = (*(processor->registers + reg + 1) & 0x80) >> 7;
    *(processor->registers) = *(processor->registers + reg) << 1 | hc;
    *(processor->registers + 1) = *(processor->registers + reg + 1) << 1 | c;
    processor->programCounter += 2;
}

void SVExec_operation_rotr8(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t reg = (uint8_t) (*mem & 0b00001111);
    bool c = processor->carryFlag = *(processor->registers + reg) & 0x01;
    *(processor->registers + 1) = *(processor->registers + reg) >> 1 | (c << 7);
    processor->programCounter += 2;
}

void SVExec_operation_rotr16(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t reg = (uint8_t) (*mem & 0b00001111);
    bool c = processor->carryFlag = *(processor->registers + reg + 1) & 1;
    bool hc = *(processor->registers + reg) & 1;
    *(processor->registers + 1) = *(processor->registers + reg + 1) >> 1 | (hc << 7);
    *(processor->registers) = *(processor->registers + reg) >> 1 | (c << 7);
    processor->programCounter += 2;
}

void SVExec_operation_jumpaddr(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint16_t addr = (*mem << 8) + *(mem + 1);
    processor->programCounter = addr;
}

void SVExec_operation_jumpreg(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t reg = (uint8_t) (*mem & 0b00001111);
    processor->programCounter = *(processor->registers + reg);
}

void SVExec_operation_jumpz(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    if(processor->zeroFlag)
    {
        uint16_t addr = (*mem << 8) + *(mem + 1);
        processor->programCounter = addr;
    }
    else
        processor->programCounter += 3;
}

void SVExec_operation_jumprz(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    if(processor->zeroFlag)
    {
        uint8_t reg = (uint8_t) (*mem & 0b00001111);
        processor->programCounter = *(processor->registers + reg);
    }
    else
        processor->programCounter += 3;
}

void SVExec_operation_jumpnz(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    if(!processor->zeroFlag)
    {
        uint16_t addr = (*mem << 8) + *(mem + 1);
        processor->programCounter = addr;
    }
    else
        processor->programCounter += 3;
}

void SVExec_operation_jumprnz(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    if(!processor->zeroFlag)
    {
        uint8_t reg = (uint8_t) (*mem & 0b00001111);
        processor->programCounter = *(processor->registers + reg);
    }
    else
        processor->programCounter += 3;
}

void SVExec_operation_jumpc(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    if(processor->carryFlag)
    {
        uint16_t addr = (*mem << 8) + *(mem + 1);
        processor->programCounter = addr;
    }
    else
        processor->programCounter += 3;
}

void SVExec_operation_jumprc(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    if(processor->carryFlag)
    {
        uint8_t reg = (uint8_t) (*mem & 0b00001111);
        processor->programCounter = *(processor->registers + reg);
    }
    else
        processor->programCounter += 3;
}

void SVExec_operation_jumpnc(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    if(!processor->carryFlag)
    {
        uint16_t addr = (*mem << 8) + *(mem + 1);
        processor->programCounter = addr;
    }
    else
        processor->programCounter += 3;
}

void SVExec_operation_jumprnc(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    if(!processor->carryFlag)
    {
        uint8_t reg = (uint8_t) (*mem & 0b00001111);
        processor->programCounter = *(processor->registers + reg);
    }
    else
        processor->programCounter += 3;
}

void SVExec_operation_jumpcond(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    if(processor->conditionFlag)
    {
        uint16_t addr = (*mem << 8) + *(mem + 1);
        processor->programCounter = addr;
    }
    else
        processor->programCounter += 3;
}

void SVExec_operation_jumprcond(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    if(processor->conditionFlag)
    {
        uint8_t reg = (uint8_t) (*mem & 0b00001111);
        processor->programCounter = *(processor->registers + reg);
    }
    else
        processor->programCounter += 3;
}

void SVExec_operation_jumpncond(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    if(!processor->conditionFlag)
    {
        uint16_t addr = (*mem << 8) + *(mem + 1);
        processor->programCounter = addr;
    }
    else
        processor->programCounter += 3;
}

void SVExec_operation_jumprncond(SVExec_Processor *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    if(!processor->conditionFlag)
    {
        uint8_t reg = (uint8_t) (*mem & 0b00001111);
        processor->programCounter = *(processor->registers + reg);
    }
    else
        processor->programCounter += 3;
}

void SVExec_operation_cmpequ8(struct SVExec_Processor_ *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t regA = (uint8_t) (*mem & 0b00001111);
    uint8_t regB = (uint8_t) ((*mem & 0b11110000) >> 4);
    processor->conditionFlag = *(processor->registers + regA) == *(processor->registers + regB);
    processor->programCounter += 2;
}

void SVExec_operation_cmpequ16(struct SVExec_Processor_ *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t regA = (uint8_t) (*mem & 0b00001110);
    uint8_t regB = (uint8_t) ((*mem & 0b11100000) >> 4);
    uint16_t valA = (*(processor->registers + regA) << 8) + *(processor->registers + regA + 1);
    uint16_t valB = (*(processor->registers + regB) << 8) + *(processor->registers + regB + 1);
    processor->conditionFlag = valA == valB;
    processor->programCounter += 2;
}

void SVExec_operation_cmpnequ8(struct SVExec_Processor_ *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t regA = (uint8_t) (*mem & 0b00001111);
    uint8_t regB = (uint8_t) ((*mem & 0b11110000) >> 4);
    processor->conditionFlag = *(processor->registers + regA) != *(processor->registers + regB);
    processor->programCounter += 2;
}

void SVExec_operation_cmpnequ16(struct SVExec_Processor_ *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t regA = (uint8_t) (*mem & 0b00001110);
    uint8_t regB = (uint8_t) ((*mem & 0b11100000) >> 4);
    uint16_t valA = (*(processor->registers + regA) << 8) + *(processor->registers + regA + 1);
    uint16_t valB = (*(processor->registers + regB) << 8) + *(processor->registers + regB + 1);
    processor->conditionFlag = valA != valB;
    processor->programCounter += 2;
}

void SVExec_operation_cmpsup8(struct SVExec_Processor_ *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t regA = (uint8_t) (*mem & 0b00001111);
    uint8_t regB = (uint8_t) ((*mem & 0b11110000) >> 4);
    processor->conditionFlag = *(processor->registers + regA) > *(processor->registers + regB);
    processor->programCounter += 2;
}

void SVExec_operation_cmpsup16(struct SVExec_Processor_ *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t regA = (uint8_t) (*mem & 0b00001110);
    uint8_t regB = (uint8_t) ((*mem & 0b11100000) >> 4);
    uint16_t valA = (*(processor->registers + regA) << 8) + *(processor->registers + regA + 1);
    uint16_t valB = (*(processor->registers + regB) << 8) + *(processor->registers + regB + 1);
    processor->conditionFlag = valA > valB;
    processor->programCounter += 2;
}

void SVExec_operation_cmpnsup8(struct SVExec_Processor_ *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t regA = (uint8_t) (*mem & 0b00001111);
    uint8_t regB = (uint8_t) ((*mem & 0b11110000) >> 4);
    processor->conditionFlag = *(processor->registers + regA) <= *(processor->registers + regB);
    processor->programCounter += 2;
}

void SVExec_operation_cmpnsup16(struct SVExec_Processor_ *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t regA = (uint8_t) (*mem & 0b00001110);
    uint8_t regB = (uint8_t) ((*mem & 0b11100000) >> 4);
    uint16_t valA = (*(processor->registers + regA) << 8) + *(processor->registers + regA + 1);
    uint16_t valB = (*(processor->registers + regB) << 8) + *(processor->registers + regB + 1);
    processor->conditionFlag = valA <= valB;
    processor->programCounter += 2;
}

void SVExec_operation_cmpinf8(struct SVExec_Processor_ *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t regA = (uint8_t) (*mem & 0b00001111);
    uint8_t regB = (uint8_t) ((*mem & 0b11110000) >> 4);
    processor->conditionFlag = *(processor->registers + regA) < *(processor->registers + regB);
    processor->programCounter += 2;
}

void SVExec_operation_cmpinf16(struct SVExec_Processor_ *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t regA = (uint8_t) (*mem & 0b00001110);
    uint8_t regB = (uint8_t) ((*mem & 0b11100000) >> 4);
    uint16_t valA = (*(processor->registers + regA) << 8) + *(processor->registers + regA + 1);
    uint16_t valB = (*(processor->registers + regB) << 8) + *(processor->registers + regB + 1);
    processor->conditionFlag = valA < valB;
    processor->programCounter += 2;
}

void SVExec_operation_cmpninf8(struct SVExec_Processor_ *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t regA = (uint8_t) (*mem & 0b00001111);
    uint8_t regB = (uint8_t) ((*mem & 0b11110000) >> 4);
    processor->conditionFlag = *(processor->registers + regA) >= *(processor->registers + regB);
    processor->programCounter += 2;
}

void SVExec_operation_cmpninf16(struct SVExec_Processor_ *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t regA = (uint8_t) (*mem & 0b00001110);
    uint8_t regB = (uint8_t) ((*mem & 0b11100000) >> 4);
    uint16_t valA = (*(processor->registers + regA) << 8) + *(processor->registers + regA + 1);
    uint16_t valB = (*(processor->registers + regB) << 8) + *(processor->registers + regB + 1);
    processor->conditionFlag = valA >= valB;
    processor->programCounter += 2;
}

void SVExec_operation_add8(struct SVExec_Processor_ *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t regA = (uint8_t) (*mem & 0b00001111);
    uint8_t regB = (uint8_t) ((*mem & 0b11110000) >> 4);
    uint16_t val = *(processor->registers + regA) + *(processor->registers + regB);
    *(processor->registers + 1) = (uint8_t) (val & 0xFF);
    processor->carryFlag = val & 0x100 >> 8;
    processor->programCounter += 2;
}

void SVExec_operation_add16(struct SVExec_Processor_ *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t regA = (uint8_t) (*mem & 0b00001110);
    uint8_t regB = (uint8_t) ((*mem & 0b11100000) >> 4);
    uint16_t val1 = *(processor->registers + regA + 1) + *(processor->registers + regB + 1);
    uint16_t val2 = *(processor->registers + regA) + *(processor->registers + regB) + (uint16_t) ((val1 & 0x100) >> 8);
    *(processor->registers + 1) = (uint8_t) (val1 & 0xFF);
    *(processor->registers) = (uint8_t) (val2 & 0xFF);
    processor->carryFlag = val2 & 0x100 >> 8;
    processor->programCounter += 2;
}

void SVExec_operation_sub8(struct SVExec_Processor_ *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t regA = (uint8_t) (*mem & 0b00001111);
    uint8_t regB = (uint8_t) ((*mem & 0b11110000) >> 4);
    uint16_t val = *(processor->registers + regA) - *(processor->registers + regB);
    *(processor->registers + 1) = (uint8_t) (val & 0xFF);
    processor->carryFlag = val & 0x100 >> 8;
    processor->programCounter += 2;
}

void SVExec_operation_sub16(struct SVExec_Processor_ *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t regA = (uint8_t) (*mem & 0b00001110);
    uint8_t regB = (uint8_t) ((*mem & 0b11100000) >> 4);
    uint16_t val1 = *(processor->registers + regA + 1) - *(processor->registers + regB + 1);
    uint16_t val2 = *(processor->registers + regA) - *(processor->registers + regB) - (uint16_t) ((val1 & 0x100) >> 8);
    *(processor->registers + 1) = (uint8_t) (val1 & 0xFF);
    *(processor->registers) = (uint8_t) (val2 & 0xFF);
    processor->carryFlag = val2 & 0x100 >> 8;
    processor->programCounter += 2;
}

void SVExec_operation_addc8(struct SVExec_Processor_ *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t regA = (uint8_t) (*mem & 0b00001111);
    uint8_t regB = (uint8_t) ((*mem & 0b11110000) >> 4);
    uint16_t val = *(processor->registers + regA) + *(processor->registers + regB) + processor->carryFlag;
    *(processor->registers + 1) = (uint8_t) (val & 0xFF);
    processor->carryFlag = val & 0x100 >> 8;
    processor->programCounter += 2;
}

void SVExec_operation_addc16(struct SVExec_Processor_ *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t regA = (uint8_t) (*mem & 0b00001110);
    uint8_t regB = (uint8_t) ((*mem & 0b11100000) >> 4);
    uint16_t val1 = *(processor->registers + regA + 1) + *(processor->registers + regB + 1) + processor->carryFlag;
    uint16_t val2 = *(processor->registers + regA) + *(processor->registers + regB) + (uint16_t) (val1 & 0xFF >> 8);
    *(processor->registers + 1) = (uint8_t) (val1 & 0xFF);
    processor->carryFlag = val2 & 0x100 >> 8;
    processor->programCounter += 2;
}

void SVExec_operation_subc8(struct SVExec_Processor_ *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t regA = (uint8_t) (*mem & 0b00001111);
    uint8_t regB = (uint8_t) ((*mem & 0b11110000) >> 4);
    uint16_t val = *(processor->registers + regA) - *(processor->registers + regB) - processor->carryFlag;
    *(processor->registers + 1) = (uint8_t) (val & 0xFF);
    processor->carryFlag = val & 0x100 >> 8;
    processor->programCounter += 2;
}

void SVExec_operation_subc16(struct SVExec_Processor_ *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t regA = (uint8_t) (*mem & 0b00001110);
    uint8_t regB = (uint8_t) ((*mem & 0b11100000) >> 4);
    uint16_t val1 = *(processor->registers + regA + 1) - *(processor->registers + regB + 1) - processor->carryFlag;
    uint16_t val2 = *(processor->registers + regA) - *(processor->registers + regB) - (uint16_t) (val1 & 0xFF >> 8);
    *(processor->registers + 1) = (uint8_t) (val1 & 0xFF);
    processor->carryFlag = val2 & 0x100 >> 8;
    processor->programCounter += 2;
}

void SVExec_operation_sec(struct SVExec_Processor_ *processor)
{
    processor->carryFlag = true;
    processor->programCounter++;
}

void SVExec_operation_clc(struct SVExec_Processor_ *processor)
{
    processor->carryFlag = false;
    processor->programCounter++;
}


void SVExec_operation_inc8(struct SVExec_Processor_ *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t reg = (uint8_t) (*mem & 0b00001111);
    uint16_t val = (uint16_t) (*(processor->registers + reg) + 1);
    *(processor->registers + reg) = (uint8_t) (val & 0xFF);
    processor->carryFlag = val & 0x100 >> 8;
    processor->programCounter += 2;
}

void SVExec_operation_inc16(struct SVExec_Processor_ *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t reg = (uint8_t) (*mem & 0b00001110);
    uint16_t val1 = (uint16_t) (*(processor->registers + reg + 1) + 1);
    uint16_t val2 = *(processor->registers + reg) + (uint16_t) ((val1 & 0x100) >> 8);
    *(processor->registers + reg + 1) = (uint8_t) (val1 & 0xFF);
    *(processor->registers + reg) = (uint8_t) (val2 & 0xFF);
    processor->carryFlag = val2 & 0x100 >> 8;
    processor->programCounter += 2;
}

void SVExec_operation_dec8(struct SVExec_Processor_ *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t reg = (uint8_t) (*mem & 0b00001111);
    uint16_t val = (uint16_t) (*(processor->registers + reg) - 1);
    *(processor->registers + reg) = (uint8_t) (val & 0xFF);
    processor->carryFlag = val & 0x100 >> 8;
    processor->programCounter += 2;
}

void SVExec_operation_dec16(struct SVExec_Processor_ *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t reg = (uint8_t) (*mem & 0b00001110);
    uint16_t val1 = (uint16_t) (*(processor->registers + reg + 1) - 1);
    uint16_t val2 = *(processor->registers + reg) - (uint16_t) ((val1 & 0x100) >> 8);
    *(processor->registers + reg + 1) = (uint8_t) (val1 & 0xFF);
    *(processor->registers + reg) = (uint8_t) (val2 & 0xFF);
    processor->carryFlag = val2 & 0x100 >> 8;
    processor->programCounter += 2;
}


void SVExec_operation_push8(struct SVExec_Processor_ *processor)
{
    if(processor->context->memory->stackSize == 0 ||
       processor->stackPointer >= processor->context->memory->stackStart + processor->context->memory->stackSize - 1)
    {
        processor->error = SVEXEC_ERROR_STACK;
        return;
    }
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t reg = (uint8_t) (*mem & 0b00001111);
    uint8_t *stack = SVExec_processor_getMemoryWriteInl(processor, (uint16_t) (processor->stackPointer + 1), 1);
    if(!stack)
    {
        processor->error = SVEXEC_ERROR_STACK;
        return;
    }
    *stack = processor->registers[reg];
    processor->stackPointer++;
    processor->programCounter += 2;
}

void SVExec_operation_push16(struct SVExec_Processor_ *processor)
{
    if(processor->context->memory->stackSize == 0 ||
       processor->stackPointer >= processor->context->memory->stackStart + processor->context->memory->stackSize - 2)
    {
        processor->error = SVEXEC_ERROR_STACK;
        return;
    }
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t reg = (uint8_t) (*mem & 0b00001110);
    uint8_t *stack = SVExec_processor_getMemoryWriteInl(processor, (uint16_t) (processor->stackPointer + 1), 2);
    if(!stack)
    {
        processor->error = SVEXEC_ERROR_STACK;
        return;
    }
    *stack = processor->registers[reg];
    *(stack + 1) = processor->registers[reg + 1];
    processor->stackPointer += 2;
    processor->programCounter += 2;
}

void SVExec_operation_pop8(struct SVExec_Processor_ *processor)
{
    if(processor->context->memory->stackSize == 0 ||
       processor->stackPointer <= processor->context->memory->stackStart)
    {
        processor->error = SVEXEC_ERROR_STACK;
        return;
    }
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t reg = (uint8_t) (*mem & 0b00001111);
    uint8_t *stack = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->stackPointer), 1);
    if(!stack)
    {
        processor->error = SVEXEC_ERROR_STACK;
        return;
    }
    processor->registers[reg] = *stack;
    processor->stackPointer--;
    processor->programCounter += 2;
}

void SVExec_operation_pop16(struct SVExec_Processor_ *processor)
{
    if(processor->context->memory->stackSize == 0 ||
       processor->stackPointer <= processor->context->memory->stackStart + 1)
    {
        processor->error = SVEXEC_ERROR_STACK;
        return;
    }
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    uint8_t reg = (uint8_t) (*mem & 0b00001110);
    uint8_t *stack = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->stackPointer - 1), 2);
    if(!stack)
    {
        processor->error = SVEXEC_ERROR_STACK;
        return;
    }
    processor->registers[reg + 1] = *(stack + 1);
    processor->registers[reg] = *(stack);
    processor->stackPointer -= 2;
    processor->programCounter += 2;
}

void SVExec_operation_call(struct SVExec_Processor_ *processor)
{
    if(processor->context->memory->stackSize == 0 ||
       processor->stackPointer >= processor->context->memory->stackStart + processor->context->memory->stackSize - 2)
    {
        processor->error = SVEXEC_ERROR_STACK;
        return;
    }
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 2);
    SVEXEC_OP_MEMCHK(mem);
    processor->programCounter += 3;
    uint8_t *stack = SVExec_processor_getMemoryWriteInl(processor, (uint16_t) (processor->stackPointer + 1), 2);
    if(!stack)
    {
        processor->error = SVEXEC_ERROR_STACK;
        return;
    }
    *stack = (uint8_t) ((processor->programCounter >> 8) & 0xFF);
    *(stack + 1) = (uint8_t) (processor->programCounter & 0xFF);
    processor->stackPointer += 2;
    processor->programCounter = (*mem << 8) + *(mem + 1);
}

void SVExec_operation_ret(struct SVExec_Processor_ *processor)
{
    if(processor->context->memory->stackSize == 0 ||
       processor->stackPointer <= processor->context->memory->stackStart)
    {
        processor->error = SVEXEC_ERROR_STACK;
        return;
    }
    uint8_t *stack = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->stackPointer - 1), 2);
    if(!stack)
    {
        processor->error = SVEXEC_ERROR_STACK;
        return;
    }
    processor->stackPointer -= 2;
    processor->programCounter = (uint16_t) (((*stack) << 8) + (*stack + 1));
}

void SVExec_operation_brk(struct SVExec_Processor_ *processor)
{
    processor->running = false;
    processor->programCounter++;
}

void SVExec_operation_sys(struct SVExec_Processor_ *processor)
{
    uint8_t *mem = SVExec_processor_getMemoryReadInl(processor, (uint16_t) (processor->programCounter + 1), 1);
    SVEXEC_OP_MEMCHK(mem);
    (*(processor->context->syscalls->syscalls[*mem]))(processor);
    processor->programCounter += 2;
}
