#ifndef SVEXEC_EXPORT_H
#define SVEXEC_EXPORT_H

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>


struct SVExec_Export_
{
    char *name;
    uint16_t address;
};

typedef struct SVExec_Export_ SVExec_Export;


struct SVExec_ExportList_
{
    SVExec_Export **exports;
    size_t exportCount;
};

typedef struct SVExec_ExportList_ SVExec_ExportList;


SVExec_Export* SVExec_export_create(char *name, uint16_t address);

void SVExec_export_destroy(SVExec_Export *export);


SVExec_ExportList* SVExec_exportList_create(void);

void SVExec_exportList_destroy(SVExec_ExportList *list);

void SVExec_exportList_add(SVExec_ExportList *list, SVExec_Export *export);

bool SVExec_exportList_get(SVExec_ExportList *list, char *name, uint16_t *address);


#endif //SVEXEC_EXPORT_H
