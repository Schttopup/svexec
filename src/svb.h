#ifndef SVEXEC_SVB_H
#define SVEXEC_SVB_H

#include "memory.h"
#include "export.h"


SVExec_MemoryObject* SVExec_SVB_load(uint8_t const *data, size_t size);


#endif //SVEXEC_SVB_H
