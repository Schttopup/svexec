#include "export.h"
#include <sdefs.h>
#include <string.h>


SVExec_Export* SVExec_export_create(char *name, uint16_t address)
{
    if(!name)
        return NULL;
    SVExec_Export *export = NULL;
    MMALLOC(export, SVExec_Export, 1);
    export->name = strdup(name);
    export->address = address;
    return export;
}

void SVExec_export_destroy(SVExec_Export *export)
{
    if(!export)
        return;
    FFREE(export->name);
    FFREE(export);
}


SVExec_ExportList* SVExec_exportList_create(void)
{
    SVExec_ExportList *list = NULL;
    MMALLOC(list, SVExec_ExportList, 1);
    list->exports = NULL;
    list->exportCount = 0;
    return list;
}

void SVExec_exportList_destroy(SVExec_ExportList *list)
{
    if(!list)
        return;
    for(size_t i = 0; i < list->exportCount; i++)
        SVExec_export_destroy(list->exports[i]);
    FFREE(list->exports);
    FFREE(list);
}

void SVExec_exportList_add(SVExec_ExportList *list, SVExec_Export *export)
{
    if(!list || !export)
        return;
    RREALLOC(list->exports, SVExec_Export*, list->exportCount + 1);
    list->exports[list->exportCount] = export;
    list->exportCount++;
}

bool SVExec_exportList_get(SVExec_ExportList *list, char *name, uint16_t *address)
{
    if(!list || !name)
        return 0;
    for(size_t i = 0; i < list->exportCount; i++)
    {
        if(strcmp(list->exports[i]->name, name) == 0)
        {
            (*address) = list->exports[i]->address;
            return true;
        }
    }
    return false;
}
