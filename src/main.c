#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <sdefs.h>
#include "processor.h"
#include "memory.h"
#include "context.h"
#include "operation.h"
#include "svb.h"
#include <sfile/sfile.h>


void syscall_print(SVExec_Processor *processor)
{
    int n = processor->registers[1] | (processor->registers[0] << 8);
    //printf("%d\n", n);
}

int main(int argc, char *argv[]) {
    UNUSED(argc);
    UNUSED(argv);

    static uint8_t tab[256] = {
            // Fibonacci
            0x03, 0x00, 0x01, 0x00, 0x03, 0x00, 0x01, 0x02, 0x03, 0x00, 0x01, 0x04, 0x03, 0x80, 0x00, 0x06,
            0x73, 0x01, 0x31, 0x02, 0x11, 0x04, 0x11, 0x20, 0x11, 0x42, 0x7d, 0x60, 0x6a, 0x00, 0x10, 0x72
    };

    SVExec_MemoryObject *mem = SVExec_memoryObject_create(tab, 256, 0);
    /*uint8_t *data;
    size_t size;
    SFile_loadRaw("asm.svb", &data, &size);
    SVExec_MemoryObject *mem = SVExec_SVB_load(data, size);*/
    SVExec_SyscallList *sys = SVExec_syscallList_create();
    SVExec_Processor *proc = SVExec_processor_create(sys, mem);
    SVExec_syscallList_registerSyscall(sys, syscall_print, 1);


    clock_t t1 = clock();
    long counter = 0;

    for(size_t j = 0; j < 10000000; j++)
    {
        //printf("Next\n");
        SVExec_processor_clear(proc);
        for(size_t i = 0; i < 10000; i++)
        {
            /*
            printf("@ %04x : %02x ; ", proc->programCounter, tab[proc->programCounter]);
            for(unsigned int j = 0; j < 16; j+=2)
                printf("%02x%02x ", proc->registers[j], proc->registers[j+1]);
            printf("\n");
            //*/
            if(proc->error)
            {
                printf("Error %d\n", proc->error);
                break;
            }
            if(!proc->running)
            {
                //printf("Done\n");
                break;
            }
            SVExec_processor_step(proc);
            counter++;
        }
    }



    clock_t t2 = clock();
    double t = (double)(t2 - t1) / (double)CLOCKS_PER_SEC;
    printf("%d instructions in %f secs\n", counter, t);
    printf("%f IPS, %.2f MHz\n", ((float)counter) / t, (((float)counter) / t) / 1000000.);

    SVExec_syscallList_destroy(sys);
    SVExec_memoryObject_destroy(mem);
    SVExec_processor_destroy(proc);

    return 0;
}