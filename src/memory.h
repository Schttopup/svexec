/**
 * \file memory.h
 * \brief Declares data structures and functions related to memory objects.
 * \author Schttopup
 */

#ifndef SVEXEC_MEMORY_H
#define SVEXEC_MEMORY_H

#include <stdint.h>
#include "export.h"


/**
 * \brief Memory object.
 * Contains memory block sizes and constant data.
 */
struct SVExec_MemoryObject_
{
    uint8_t *constData;         ///< Constant data
    uint16_t constDataSize;     ///< Constant data size
    uint16_t varDataSize;       ///< Variable data size
    uint16_t varDataStart;      ///< Variable data start address
    uint16_t stackStart;        ///< Stack start address
    uint16_t stackSize;         ///< Stack size
    SVExec_ExportList *exports; ///< Export list
};
/// Type name for memory objects.
typedef struct SVExec_MemoryObject_ SVExec_MemoryObject;

/**
 * \brief Creates a new memory object.
 * \param data Constant data
 * \param constDataSize Constant data size
 * \param varDataSize Variable data size
 * \return The created memory object
 */
SVExec_MemoryObject* SVExec_memoryObject_create(uint8_t const *data, uint16_t constDataSize, uint16_t varDataSize);

/**
 * \brief Destroys a memory object.
 * \param memory The current memory object
 */
void SVExec_memoryObject_destroy(SVExec_MemoryObject *memory);

#endif //SVEXEC_MEMORY_H
