/**
 * \file syscall.h
 * \brief Declares data structures and functions related to system calls.
 * \author Schttopup
 */

#ifndef SVEXEC_SYSCALL_H
#define SVEXEC_SYSCALL_H

#include <stdint.h>


struct SVExec_Processor_;

/// Type name for system calls.
typedef void (*SVExec_Syscall)(struct SVExec_Processor_*);


/**
 * System call list.
 */
struct SVExec_SyscallList_
{
    SVExec_Syscall syscalls[256];   ///< System calls
};

/// Type name for system call lists.
typedef struct SVExec_SyscallList_ SVExec_SyscallList;


/**
 * Creates a system call list.
 * \return The created list
 */
SVExec_SyscallList* SVExec_syscallList_create(void);

/**
 * Destroys a system call list.
 * \param list The list to destroy
 */
void SVExec_syscallList_destroy(SVExec_SyscallList *list);

/**
 * \brief Associates an opcode to a syscall.
 * \param todo The syscall
 * \param opcode The opcode to associate
 */
void SVExec_syscallList_registerSyscall(SVExec_SyscallList *list, SVExec_Syscall syscall, uint8_t opcode);


#endif //SVEXEC_SYSCALL_H
