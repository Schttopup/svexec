/**
 * \file processor.c
 * \brief Defines data structures and functions related to bytecode processors.
 * \author Schttopup
 */

#include "processor.h"
#include "operation.h"
#include "context.h"
#include <sdefs.h>


SVExec_Processor* SVExec_processor_create(SVExec_SyscallList *syscalls, SVExec_MemoryObject *memory)
{
    if(!syscalls || !memory)
        return NULL;
    SVExec_Processor *processor = NULL;
    MMALLOC(processor, SVExec_Processor, 1);
    processor->context = SVExec_context_create(syscalls, memory);
    SVExec_processor_clear(processor);
    return processor;
}

void SVExec_processor_destroy(SVExec_Processor *processor)
{
    if(!processor)
        return;
    SVExec_context_destroy(processor->context);
    FFREE(processor);
}

// Function is duplicated for inlining
uint8_t* SVExec_processor_getMemoryRead(SVExec_Processor *processor, uint16_t address, uint16_t size)
{
    SVExec_MemoryObject *memory = processor->context->memory;
    uint16_t varStart = memory->varDataStart;
    if(address >= varStart) // Variable block
    {
        if(address - varStart <= memory->varDataSize - size)
            return processor->context->variables + address - varStart;
    }
    else // Constant block
    {
        if(address <= memory->constDataSize - size)
            return memory->constData + address;
    }
    processor->error = SVEXEC_ERROR_MEMORY;
    return NULL;
}

// Function is duplicated for inlining
uint8_t* SVExec_processor_getMemoryWrite(SVExec_Processor *processor, uint16_t address, uint16_t size)
{
    SVExec_MemoryObject *memory = processor->context->memory;
    uint16_t varStart = memory->varDataStart;
    if(address >= varStart) // Variable block
    {
        if(address - varStart <= memory->varDataSize - size)
            return processor->context->variables + address - varStart;
    }
    else // Constant block
    {
        processor->error = SVEXEC_ERROR_MEMORY;
        return NULL;
    }
    processor->error = SVEXEC_ERROR_MEMORY;
    return NULL;
}

void SVExec_processor_step(SVExec_Processor *processor)
{
    if(!processor)
        return;
    uint8_t *opcode = SVExec_processor_getMemoryRead(processor, processor->programCounter, 1);
    if(!opcode)
    {
        processor->error = SVEXEC_ERROR_MEMORY;
        return;
    }
    SVExec_Operation op = SVExec_operations[*opcode];
    if(!op)
    {
        processor->error = SVEXEC_ERROR_OPCODE;
        return;
    }
    (*op)(processor);
}

void SVExec_processor_clear(SVExec_Processor *processor)
{
    if(!processor)
        return;
    processor->programCounter = 0;
    processor->stackPointer = processor->context->memory->stackStart;
    processor->conditionFlag = processor->carryFlag = processor->zeroFlag = false;
    for(unsigned int i = 0; i < 16; i++)
        processor->registers[i] = 0;
    processor->error = SVEXEC_ERROR_NONE;
    processor->running = true;
}

void SVExec_processor_jump(SVExec_Processor *processor, uint16_t address)
{
    if(!processor)
        return;
    uint8_t *l = SVExec_processor_getMemoryRead(processor, address, 1);
    if(l)
        processor->programCounter = address;
    else
        processor->error = SVEXEC_ERROR_MEMORY;
}
