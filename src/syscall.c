/**
 * \file syscall.c
 * \brief Defines data structures and functions related to system calls.
 * \author Schttopup
 */

#include "syscall.h"
#include <sdefs.h>



SVExec_SyscallList* SVExec_syscallList_create(void)
{
    SVExec_SyscallList *list = NULL;
    MMALLOC(list, SVExec_SyscallList, 1);
    for(unsigned int i = 0; i < 256; i++)
        list->syscalls[i] = NULL;
    return list;
}

void SVExec_syscallList_destroy(SVExec_SyscallList *list)
{
    if(!list)
        return;
    FFREE(list);
}

void SVExec_syscallList_registerSyscall(SVExec_SyscallList *list, SVExec_Syscall syscall, uint8_t opcode)
{
    if(!list)
        return;
    list->syscalls[opcode] = syscall;
}
