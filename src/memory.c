/**
 * \file memory.c
 * \brief Defines data structures and functions related to memory objects.
 * \author Schttopup
 */

#include "memory.h"
#include <sdefs.h>

SVExec_MemoryObject* SVExec_memoryObject_create(uint8_t const *data, uint16_t constDataSize, uint16_t varDataSize)
{
    if(!data)
        return NULL;
    if(constDataSize + varDataSize > 0x10000)
        return NULL;
    SVExec_MemoryObject *memory = NULL;
    MMALLOC(memory, SVExec_MemoryObject, 1);
    memory->constData = NULL;
    MMALLOC(memory->constData, uint8_t, constDataSize);
    for(unsigned int i = 0; i < constDataSize; i++)
        memory->constData[i] = data[i];
    memory->constDataSize = constDataSize;
    memory->varDataSize = varDataSize;
    memory->varDataStart = memory->constDataSize;
    memory->stackStart = 0;
    memory->stackSize = 0;
    memory->exports = SVExec_exportList_create();
    return memory;
}

void SVExec_memoryObject_destroy(SVExec_MemoryObject *memory)
{
    if(!memory)
        return;
    FFREE(memory->constData);
    SVExec_exportList_destroy(memory->exports);
    FFREE(memory);
}
