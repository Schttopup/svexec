/**
 * \file context.h
 * \brief Declares data structures and functions related to execution contexts.
 * \author Schttopup
 */

#ifndef SVEXEC_CONTEXT_H
#define SVEXEC_CONTEXT_H

#include <stdint.h>
#include "operation.h"
#include "syscall.h"
#include "memory.h"


/// Opcode / operation associations
extern SVExec_Operation const SVExec_operations[256];


/**
 * \brief Execution context.
 * Contains processor data.
 */
struct SVExec_Context_
{
    SVExec_SyscallList *syscalls;   ///< System calls
    SVExec_MemoryObject *memory;    ///< Memory object
    uint8_t *variables;             ///< Variable memory block
    void **external;                ///< External data
    uint16_t externalCount;         ///< External data count
};

/// Type name for system execution contexts.
typedef struct SVExec_Context_ SVExec_Context;


/**
 * \brief Creates a new system context.
 * \return The created context.
 */
SVExec_Context* SVExec_context_create(SVExec_SyscallList *syscalls, SVExec_MemoryObject *memory);

/**
 * \brief Destroys a system context.
 * \param context The current context.
 */
void SVExec_context_destroy(SVExec_Context *context);

/**
 * Creates external data.
 * \param context The execution context
 * \param data The data
 * \return A handle on the data
 */
uint16_t SVExec_context_createExternal(SVExec_Context *context, void *data);

/**
 * Gets external data.
 * \param context The execution context
 * \param external The handle on the data
 * \return The data, or NULL if no data was found
 */
void* SVExec_context_getExternal(SVExec_Context *context, uint16_t external);

/**
 * Destroys external data.
 * \param context The execution context
 * \param external The handle on the data
 */
void SVExec_context_destroyExternal(SVExec_Context *context, uint16_t external);



#endif //SVEXEC_CONTEXT_H
