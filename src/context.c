/**
 * \file context.c
 * \brief Defines data structures and functions related to execution contexts.
 * \author Schttopup
 */

#include "context.h"
#include <sdefs.h>
#include "operation.h"
#include "syscall.h"


SVExec_Operation const SVExec_operations[256] = {
        [0x00] = SVExec_operation_nop,

        [0x02] = SVExec_operation_ldimm8,
        [0x03] = SVExec_operation_ldimm16,
        [0x04] = SVExec_operation_ldmem8,
        [0x05] = SVExec_operation_ldmem16,
        [0x06] = SVExec_operation_ldmem8r,
        [0x07] = SVExec_operation_ldmem16r,
        [0x08] = SVExec_operation_ldmem8a,
        [0x09] = SVExec_operation_ldmem16a,
        [0x0A] = SVExec_operation_stmem8,
        [0x0B] = SVExec_operation_stmem16,
        [0x0C] = SVExec_operation_stmem8r,
        [0x0D] = SVExec_operation_stmem16r,
        [0x0E] = SVExec_operation_stmem8a,
        [0x0F] = SVExec_operation_stmem16a,

        [0x10] = SVExec_operation_mvreg8,
        [0x11] = SVExec_operation_mvreg16,
        [0x12] = SVExec_operation_mvpc,
        [0x13] = SVExec_operation_mvsp,

        [0x20] = SVExec_operation_not8,
        [0x21] = SVExec_operation_not16,
        [0x22] = SVExec_operation_or8,
        [0x23] = SVExec_operation_or16,
        [0x24] = SVExec_operation_and8,
        [0x25] = SVExec_operation_and16,
        [0x26] = SVExec_operation_xor8,
        [0x27] = SVExec_operation_xor16,
        [0x28] = SVExec_operation_shl8,
        [0x29] = SVExec_operation_shl16,
        [0x2A] = SVExec_operation_shr8,
        [0x2B] = SVExec_operation_shr16,
        [0x2C] = SVExec_operation_rotl8,
        [0x2D] = SVExec_operation_rotl16,
        [0x2E] = SVExec_operation_rotr8,
        [0x2F] = SVExec_operation_rotr16,

        [0x30] = SVExec_operation_add8,
        [0x31] = SVExec_operation_add16,
        [0x32] = SVExec_operation_sub8,
        [0x33] = SVExec_operation_sub16,
        [0x34] = SVExec_operation_addc8,
        [0x35] = SVExec_operation_addc16,
        [0x36] = SVExec_operation_subc8,
        [0x37] = SVExec_operation_subc16,

        [0x38] = SVExec_operation_sec,
        [0x39] = SVExec_operation_clc,
        [0x3A] = SVExec_operation_inc8,
        [0x3B] = SVExec_operation_inc16,
        [0x3C] = SVExec_operation_dec8,
        [0x3D] = SVExec_operation_dec16,

        [0x40] = SVExec_operation_push8,
        [0x41] = SVExec_operation_push16,
        [0x42] = SVExec_operation_pop8,
        [0x43] = SVExec_operation_pop16,

        [0x60] = SVExec_operation_jumpaddr,
        [0x61] = SVExec_operation_jumpreg,
        [0x62] = SVExec_operation_jumpz,
        [0x63] = SVExec_operation_jumprz,
        [0x64] = SVExec_operation_jumpnz,
        [0x65] = SVExec_operation_jumprnz,
        [0x66] = SVExec_operation_jumpc,
        [0x67] = SVExec_operation_jumprc,
        [0x68] = SVExec_operation_jumpnc,
        [0x69] = SVExec_operation_jumprnc,
        [0x6A] = SVExec_operation_jumpcond,
        [0x6B] = SVExec_operation_jumprcond,
        [0x6C] = SVExec_operation_jumpncond,
        [0x6D] = SVExec_operation_jumprncond,

        [0x70] = SVExec_operation_call,
        [0x71] = SVExec_operation_ret,
        [0x72] = SVExec_operation_brk,
        [0x73] = SVExec_operation_sys,

        [0x74] = SVExec_operation_cmpequ8,
        [0x75] = SVExec_operation_cmpequ16,
        [0x76] = SVExec_operation_cmpnequ8,
        [0x77] = SVExec_operation_cmpnequ16,
        [0x78] = SVExec_operation_cmpsup8,
        [0x79] = SVExec_operation_cmpsup16,
        [0x7A] = SVExec_operation_cmpnsup8,
        [0x7B] = SVExec_operation_cmpnsup16,
        [0x7C] = SVExec_operation_cmpinf8,
        [0x7D] = SVExec_operation_cmpinf16,
        [0x7E] = SVExec_operation_cmpninf8,
        [0x7F] = SVExec_operation_cmpninf16
};


SVExec_Context* SVExec_context_create(SVExec_SyscallList *syscalls, SVExec_MemoryObject *memory)
{
    if(!memory)
        return NULL;
    SVExec_Context *context = NULL;
    MMALLOC(context, SVExec_Context, 1);
    context->syscalls = syscalls;
    context->memory = memory;
    context->variables = NULL;
    MMALLOC(context->variables, uint8_t, memory->varDataSize);
    for(unsigned int i = 0; i < memory->varDataSize; i++)
        context->variables[i] = 0;
    context->external = NULL;
    context->externalCount = 0;
    return context;
}

void SVExec_context_destroy(SVExec_Context *context)
{
    if(!context)
        return;
    FFREE(context->variables);
    FFREE(context->external);
    FFREE(context);
}

uint16_t SVExec_context_createExternal(SVExec_Context *context, void *data)
{
    if(!context || !data)
        return 0;
    uint16_t handle = 0;
    for(unsigned int i = 0; i < context->externalCount; i++)
    {
        if(context->external[i] == NULL)
            handle = (uint16_t)(i + 1);
    }
    if(!handle)
    {
        context->externalCount++;
        RREALLOC(context->external, void*, context->externalCount);
        context->external[context->externalCount - 1] = data;
        handle = context->externalCount;
    }
    return handle;
}

void* SVExec_context_getExternal(SVExec_Context *context, uint16_t external)
{
    if(!context || !external)
        return NULL;
    if(external >= context->externalCount + 1)
        return NULL;
    return context->external[external];
}

void SVExec_context_destroyExternal(SVExec_Context *context, uint16_t external)
{
    if(!context || !external)
        return;
    if(external >= context->externalCount + 1)
        return;
    context->external[external] = NULL;
}

