/**
 * \file processor.h
 * \brief Declares data structures and functions related to bytecode processors.
 * \author Schttopup
 */

#ifndef SVEXEC_PROCESSOR_H
#define SVEXEC_PROCESSOR_H

#include <stdint.h>
#include <stdbool.h>
#include <sdefs.h>
#include "memory.h"
#include "context.h"


/**
 * \brief Processor error types.
 */
enum SVExec_ProcessorError_
{
    SVEXEC_ERROR_NONE,      ///< No error
    SVEXEC_ERROR_MEMORY,    ///< Memory fault
    SVEXEC_ERROR_STACK,     ///< Stack fault
    SVEXEC_ERROR_OPCODE     ///< Invalid opcode
};

/// Type name for processor error types.
typedef enum SVExec_ProcessorError_ SVExec_ProcessorError;

/**
 * \brief Bytecode processor.
 * Contains all the data regarding the processor state.
 */
struct SVExec_Processor_
{
    SVExec_Context *context;        ///< Execution context
    uint8_t registers[16];          ///< Processor registers
    uint16_t programCounter;        ///< Processor program counter register
    uint16_t stackPointer;          ///< Processor stack pointer register
    bool carryFlag;                 ///< Carry flag
    bool zeroFlag;                  ///< Zero flag
    bool conditionFlag;             ///< Conditional flag
    bool running;                   ///< Processor running state
    SVExec_ProcessorError error;    ///< Processor error state
};

/// Type name for bytecode processors.
typedef struct SVExec_Processor_ SVExec_Processor;


/**
 * \brief Creates a new processor.
 * \param context The system execution context
 * \param memory The memory object
 * \return The created processor
 */
SVExec_Processor* SVExec_processor_create(SVExec_SyscallList *syscalls, SVExec_MemoryObject *memory);

/**
 * \brief Destroys a processor.
 * \param processor The current processor
 */
void SVExec_processor_destroy(SVExec_Processor *processor);

/**
 * \brief Gets a memory pointer corresponding to a readable memory space of a given size.
 * \param processor The current processor
 * \param address The address to look at
 * \param size The size of the memory space
 * \return A pointer to the corresponding space, or NULL if the address is invalid.
 */
uint8_t* SVExec_processor_getMemoryRead(SVExec_Processor *processor, uint16_t address, uint16_t size);

/**
 * \brief Gets a memory pointer corresponding to a writable memory space of a given size.
 * \param processor The current processor
 * \param address The address to look at
 * \param size The size of the memory space
 * \return A pointer to the corresponding space, or NULL if the address is invalid.
 */
uint8_t* SVExec_processor_getMemoryWrite(SVExec_Processor *processor, uint16_t address, uint16_t size);

/**
 * \brief Executes one instruction.
 * \param processor The current processor
 */
void SVExec_processor_step(SVExec_Processor *processor);

/**
 * \brief Clears the processor's state.
 * \param processor The current processor
 */
void SVExec_processor_clear(SVExec_Processor *processor);

/**
 * \brief Jumps to an address.
 * \param processor The current processor
 * \param address The location to jump to
 */
void SVExec_processor_jump(SVExec_Processor *processor, uint16_t address);

#endif //SVEXEC_PROCESSOR_H
