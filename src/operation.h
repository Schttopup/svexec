/**
 * \file operation.h
 * \brief Declares data structures and functions related to bytecode operations.
 * \author Schttopup
 */

#ifndef SVEXEC_OPERATION_H
#define SVEXEC_OPERATION_H

#include <stdint.h>


struct SVExec_Processor_;

/// Type name for operation functions.
typedef void (*SVExec_Operation)(struct SVExec_Processor_*);


/**
 * \brief No-op.
 * Does not perform any change to the processor state.
 * \param processor The current processor
 */
void SVExec_operation_nop(struct SVExec_Processor_ *processor);

/**
 * \brief Load immediate 8-bit value.
 * Loads an immediate 8-bit value to the AL register.
 * \param processor The current processor
 */
void SVExec_operation_ldimm8(struct SVExec_Processor_ *processor);

/**
 * \brief Load immediate 16-bit value.
 * Loads an immediate 16-bit value to the A register.
 * \param processor The current processor
 */
void SVExec_operation_ldimm16(struct SVExec_Processor_ *processor);

/**
 * \brief Load 8-bit value.
 * Loads an 8-bit value at a memory location to a register.
 * \param processor The current processor
 */
void SVExec_operation_ldmem8(struct SVExec_Processor_ *processor);

/**
 * \brief Load 16-bit value.
 * Loads a 16-bit value at a memory location to a register.
 * \param processor The current processor
 */
void SVExec_operation_ldmem16(struct SVExec_Processor_ *processor);

/**
 * \brief Store 8-bit value.
 * Stores an 8-bit value at a memory location from a register.
 * \param processor The current processor
 */
void SVExec_operation_stmem8(struct SVExec_Processor_ *processor);

/**
 * \brief Store 16-bit value.
 * Stores a 16-bit value at a memory location from a register.
 * \param processor The current processor
 */
void SVExec_operation_stmem16(struct SVExec_Processor_ *processor);

/**
 * \brief Load 8-bit value at register.
 * Loads an 8-bit value at the memory location of a register to a register.
 * \param processor The current processor
 */
void SVExec_operation_ldmem8r(struct SVExec_Processor_ *processor);

/**
 * \brief Load 16-bit value at register.
 * Loads a 16-bit value at the memory location of a register to a register.
 * \param processor The current processor
 */
void SVExec_operation_ldmem16r(struct SVExec_Processor_ *processor);

/**
 * \brief Store 8-bit value at register.
 * Stores an 8-bit value at the memory location of a register from a register.
 * \param processor The current processor
 */
void SVExec_operation_stmem8r(struct SVExec_Processor_ *processor);

/**
 * \brief Store 16-bit value at register.
 * Stores a 16-bit value at the memory location of a register from a register.
 * \param processor The current processor
 */
void SVExec_operation_stmem16r(struct SVExec_Processor_ *processor);

/**
 * \brief Load 8-bit value at register.
 * Loads an 8-bit value at the address of a memory location of a register to a register.
 * \param processor The current processor
 */
void SVExec_operation_ldmem8a(struct SVExec_Processor_ *processor);

/**
 * \brief Load 16-bit value at register.
 * Loads a 16-bit value at the address of a memory location of a register to a register.
 * \param processor The current processor
 */
void SVExec_operation_ldmem16a(struct SVExec_Processor_ *processor);

/**
 * \brief Store 8-bit value at register.
 * Stores an 8-bit value at the address of a memory location of a register from a register.
 * \param processor The current processor
 */
void SVExec_operation_stmem8a(struct SVExec_Processor_ *processor);

/**
 * \brief Store 16-bit value at register.
 * Stores a 16-bit value at the address of a memory location of a register from a register.
 * \param processor The current processor
 */
void SVExec_operation_stmem16a(struct SVExec_Processor_ *processor);

/**
 * \brief Move 8-bit register
 * Moves an 8-bit value from a register to another.
 * \param processor The current processor
 */
void SVExec_operation_mvreg8(struct SVExec_Processor_ *processor);

/**
 * \brief Move 16-bit register
 * Moves a 16-bit value from a register to another.
 * \param processor The current processor
 */
void SVExec_operation_mvreg16(struct SVExec_Processor_ *processor);

/**
 * \brief Move program counter
 * Moves the program counter to a regular register.
 * \param processor The current processor
 */
void SVExec_operation_mvpc(struct SVExec_Processor_ *processor);

/**
 * \brief Move stack pointer
 * Moves the stack pointer to a regular register.
 * \param processor The current processor
 */
void SVExec_operation_mvsp(struct SVExec_Processor_ *processor);

/**
 * \brief Bitwise 8-bit not
 * Stores the 8-bit negation of a register in the AL register.
 * \param processor The current processor
 */
void SVExec_operation_not8(struct SVExec_Processor_ *processor);

/**
 * \brief Bitwise 16-bit not
 * Stores the 16-bit negation of a register in the A register.
 * \param processor The current processor
 */
void SVExec_operation_not16(struct SVExec_Processor_ *processor);

/**
 * \brief Bitwise 8-bit or
 * Stores the 8-bit disjunction of a register in the AL register.
 * \param processor The current processor
 */
void SVExec_operation_or8(struct SVExec_Processor_ *processor);

/**
 * \brief Bitwise 16-bit or
 * Stores the 16-bit disjunction of a register in the A register.
 * \param processor The current processor
 */
void SVExec_operation_or16(struct SVExec_Processor_ *processor);

/**
 * \brief Bitwise 8-bit and
 * Stores the 8-bit conjunction of a register in the AL register.
 * \param processor The current processor
 */
void SVExec_operation_and8(struct SVExec_Processor_ *processor);

/**
 * \brief Bitwise 16-bit and
 * Stores the 16-bit conjunction of a register in the A register.
 * \param processor The current processor
 */
void SVExec_operation_and16(struct SVExec_Processor_ *processor);

/**
 * \brief Bitwise 8-bit xor
 * Stores the 8-bit exclusive disjunction of a register in the AL register.
 * \param processor The current processor
 */
void SVExec_operation_xor8(struct SVExec_Processor_ *processor);

/**
 * \brief Bitwise 16-bit xor
 * Stores the 16-bit exclusive disjunction of a register in the A register.
 * \param processor The current processor
 */
void SVExec_operation_xor16(struct SVExec_Processor_ *processor);

/**
 * \brief Left 8-bit shift
 * Shifts a 8-bit register to the left and stores the result in the AL register.
 * \param processor The current processor
 */
void SVExec_operation_shl8(struct SVExec_Processor_ *processor);

/**
 * \brief Left 16-bit shift
 * Shifts a 16-bit register to the left and stores the result in the A register.
 * \param processor The current processor
 */
void SVExec_operation_shl16(struct SVExec_Processor_ *processor);

/**
 * \brief Right 8-bit shift
 * Shifts a 8-bit register to the right and stores the result in the AL register.
 * \param processor The current processor
 */
void SVExec_operation_shr8(struct SVExec_Processor_ *processor);

/**
 * \brief Right 16-bit shift
 * Shifts a 16-bit register to the right and stores the result in the A register.
 * \param processor The current processor
 */
void SVExec_operation_shr16(struct SVExec_Processor_ *processor);

/**
 * \brief Left 8-bit rotate
 * Rotates a 8-bit register to the left and stores the result in the AL register.
 * \param processor The current processor
 */
void SVExec_operation_rotl8(struct SVExec_Processor_ *processor);

/**
 * \brief Left 16-bit rotate
 * Rotates a 16-bit register to the left and stores the result in the A register.
 * \param processor The current processor
 */
void SVExec_operation_rotl16(struct SVExec_Processor_ *processor);

/**
 * \brief Right 8-bit rotate
 * Rotates a 8-bit register to the right and stores the result in the AL register.
 * \param processor The current processor
 */
void SVExec_operation_rotr8(struct SVExec_Processor_ *processor);

/**
 * \brief Right 16-bit rotate
 * Rotates a 16-bit register to the right and stores the result in the A register.
 * \param processor The current processor
 */
void SVExec_operation_rotr16(struct SVExec_Processor_ *processor);

/**
 * \brief Jump immediate address
 * Jumps at the immediate address.
 * \param processor The current processor
 */
void SVExec_operation_jumpaddr(struct SVExec_Processor_ *processor);

/**
 * \brief Jump immediate register
 * Jumps at the address given by register.
 * \param processor The current processor
 */
void SVExec_operation_jumpreg(struct SVExec_Processor_ *processor);

/**
 * \brief Jump if zero
 * Jumps at the address if the zero flag is set.
 * \param processor The current processor
 */
void SVExec_operation_jumpz(struct SVExec_Processor_ *processor);

/**
 * \brief Jump relative if zero
 * Jumps at the address if the zero flag is set.
 * \param processor The current processor
 */
void SVExec_operation_jumprz(struct SVExec_Processor_ *processor);

/**
 * \brief Jump if not zero
 * Jumps at the address if the zero flag is not set.
 * \param processor The current processor
 */
void SVExec_operation_jumpnz(struct SVExec_Processor_ *processor);

/**
 * \brief Jump relative if not zero
 * Jumps at the relative address if the zero flag is not set.
 * \param processor The current processor
 */
void SVExec_operation_jumprnz(struct SVExec_Processor_ *processor);

/**
 * \brief Jump if carry
 * Jumps at the address if the carry flag is set.
 * \param processor The current processor
 */
void SVExec_operation_jumpc(struct SVExec_Processor_ *processor);

/**
 * \brief Jump relative if carry
 * Jumps at the relative address if the carry flag is set.
 * \param processor The current processor
 */
void SVExec_operation_jumprc(struct SVExec_Processor_ *processor);

/**
 * \brief Jump if not carry
 * Jumps at the address if the carry flag is not set.
 * \param processor The current processor
 */
void SVExec_operation_jumpnc(struct SVExec_Processor_ *processor);

/**
 * \brief Jump relative if not carry
 * Jumps at the relative address if the carry flag is not set.
 * \param processor The current processor
 */
void SVExec_operation_jumprnc(struct SVExec_Processor_ *processor);

/**
 * \brief Jump if condition
 * Jumps at the address if the conditional flag is set.
 * \param processor The current processor
 */
void SVExec_operation_jumpcond(struct SVExec_Processor_ *processor);

/**
 * \brief Jump relative if condition
 * Jumps at the relative address if the conditional flag is set.
 * \param processor The current processor
 */
void SVExec_operation_jumprcond(struct SVExec_Processor_ *processor);

/**
 * \brief Jump if not condition
 * Jumps at the relative address if the conditional flag is set.
 * \param processor The current processor
 */
void SVExec_operation_jumpncond(struct SVExec_Processor_ *processor);

/**
 * \brief Jump relative if not condition
 * Jumps at the relative address if the conditional flag is not set.
 * \param processor The current processor
 */
void SVExec_operation_jumprncond(struct SVExec_Processor_ *processor);

/**
 * \brief Compare equal 8
 * Compare 8-bit registers for equality.
 * \param processor
 */
void SVExec_operation_cmpequ8(struct SVExec_Processor_ *processor);

/**
 * \brief Compare equal 16
 * Compare 16-bit registers for equality.
 * \param processor
 */
void SVExec_operation_cmpequ16(struct SVExec_Processor_ *processor);

/**
 * \brief Compare not equal 8
 * Compare 8-bit registers for inequality.
 * \param processor
 */
void SVExec_operation_cmpnequ8(struct SVExec_Processor_ *processor);

/**
 * \brief Compare not equal 16
 * Compare 16-bit registers for inequality.
 * \param processor
 */
void SVExec_operation_cmpnequ16(struct SVExec_Processor_ *processor);

/**
 * \brief Compare superior 8
 * Compare 8-bit registers for strict superiority.
 * \param processor
 */
void SVExec_operation_cmpsup8(struct SVExec_Processor_ *processor);

/**
 * \brief Compare superior 16
 * Compare 16-bit registers for strict superiority.
 * \param processor
 */
void SVExec_operation_cmpsup16(struct SVExec_Processor_ *processor);

/**
 * \brief Compare not superior 8
 * Compare 8-bit registers for inferiority.
 * \param processor
 */
void SVExec_operation_cmpnsup8(struct SVExec_Processor_ *processor);

/**
 * \brief Compare not superior 16
 * Compare 16-bit registers for inferiority.
 * \param processor
 */
void SVExec_operation_cmpnsup16(struct SVExec_Processor_ *processor);

/**
 * \brief Compare inferior 8
 * Compare 8-bit registers for strict inferiority.
 * \param processor
 */
void SVExec_operation_cmpinf8(struct SVExec_Processor_ *processor);

/**
 * \brief Compare inferior 16
 * Compare 16-bit registers for strict inferiority.
 * \param processor
 */
void SVExec_operation_cmpinf16(struct SVExec_Processor_ *processor);

/**
 * \brief Compare not inferior 8
 * Compare 8-bit registers for superiority.
 * \param processor
 */
void SVExec_operation_cmpninf8(struct SVExec_Processor_ *processor);

/**
 * \brief Compare not inferior 16
 * Compare 16-bit registers for superiority.
 * \param processor
 */
void SVExec_operation_cmpninf16(struct SVExec_Processor_ *processor);

/**
 * \brief Add 8-bit
 * Adds two 8-bit numbers from registers.
 * \param processor The current processor
 */
void SVExec_operation_add8(struct SVExec_Processor_ *processor);

/**
 * \brief Add 16-bit
 * Adds two 16-bit numbers from registers.
 * \param processor The current processor
 */
void SVExec_operation_add16(struct SVExec_Processor_ *processor);

/**
 * \brief Subtract 8-bit
 * Subtracts two 8-bit numbers from registers.
 * \param processor The current processor
 */
void SVExec_operation_sub8(struct SVExec_Processor_ *processor);

/**
 * \brief Subtract 16-bit
 * Subtracts two 16-bit numbers from registers.
 * \param processor The current processor
 */
void SVExec_operation_sub16(struct SVExec_Processor_ *processor);

/**
 * \brief Add 8-bit carry
 * Adds two 8-bit numbers from registers with carry
 * \param processor The current processor
 */
void SVExec_operation_addc8(struct SVExec_Processor_ *processor);

/**
 * \brief Add 16-bit carry
 * Adds two 8-bit numbers from registers with carry
 * \param processor The current processor
 */
void SVExec_operation_addc16(struct SVExec_Processor_ *processor);

/**
 * \brief Subtract 8-bit carry
 * Subtratcs two 8-bit numbers from registers with carry
 * \param processor The current processor
 */
void SVExec_operation_subc8(struct SVExec_Processor_ *processor);

/**
 * \brief Subtract 16-bit carry
 * Subtracts two 16-bit numbers from registers with carry
 * \param processor The current processor
 */
void SVExec_operation_subc16(struct SVExec_Processor_ *processor);

/**
 * \brief Set carry
 * Sets the carry flag
 * \param processor The current processor
 */
void SVExec_operation_sec(struct SVExec_Processor_ *processor);

/**
 * \brief Clear carry
 * Clears the carry flag
 * \param processor The current processor
 */
void SVExec_operation_clc(struct SVExec_Processor_ *processor);

/**
 * \brief Increment 8-bit
 * Increments an 8-bit register
 * \param processor The current processor
 */
void SVExec_operation_inc8(struct SVExec_Processor_ *processor);

/**
 * \brief Increment 16-bit
 * Increments a 16-bit register
 * \param processor The current processor
 */
void SVExec_operation_inc16(struct SVExec_Processor_ *processor);

/**
 * \brief Decrement 8-bit
 * Decrements an 8-bit register
 * \param processor The current processor
 */
void SVExec_operation_dec8(struct SVExec_Processor_ *processor);

/**
 * \brief Decrement 16-bit
 * Decrements a 16-bit register
 * \param processor The current processor
 */
void SVExec_operation_dec16(struct SVExec_Processor_ *processor);

/**
 * \brief Push 8-bit
 * Pushes an 8-bit register on the stack.
 * \param processor The current processor
 */
void SVExec_operation_push8(struct SVExec_Processor_ *processor);

/**
 * \brief Push 16-bit
 * Pushes a 16-bit register from the stack.
 * \param processor The current processor
 */
void SVExec_operation_push16(struct SVExec_Processor_ *processor);

/**
 * \brief Pop 8-bit
 * Pops an 8-bit register on the stack.
 * \param processor The current processor
 */
void SVExec_operation_pop8(struct SVExec_Processor_ *processor);

/**
 * \brief Pop 16-bit
 * Pops a 16-bit register from the stack.
 * \param processor The current processor
 */
void SVExec_operation_pop16(struct SVExec_Processor_ *processor);

/**
 * \brief Call subroutine
 * Pushes the program counter on the stack and jumps to the address.
 * \param processor The current processor
 */
void SVExec_operation_call(struct SVExec_Processor_ *processor);

/**
 * \brief Return
 * Restores the program counter from the stack.
 * \param processor The current processor
 */
void SVExec_operation_ret(struct SVExec_Processor_ *processor);

/**
 * \brief Break
 * Stops the execution.
 * \param processor The current processor
 */
void SVExec_operation_brk(struct SVExec_Processor_ *processor);

/**
 * \brief Syscall
 * Calls an external function.
 * \param processor The current processor
 */
void SVExec_operation_sys(struct SVExec_Processor_ *processor);


#endif //SVEXEC_OPERATION_H
